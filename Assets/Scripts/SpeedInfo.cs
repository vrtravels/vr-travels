﻿using UnityEngine;
using System.Collections;
using System;

public class SpeedInfo
{
	public Single Instant { get; set; }

	public Single Speed { get; set; }
}
