﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;

public class Launch : MonoBehaviour
{

	public Single MaxDegrees;
	public Single StartAt;
	public Single StopAt;

	public Camera Camera { get; set; }

	private Vector3 LaunchPosition { get; set; }

	private Rigidbody RB { get; set; }

	private AudioSource IgnitionSound { get; set; }

	private AudioSource BurnSound { get; set; }

	private AudioSource IntroPlayer { get; set; }

	private AudioSource OutroPlayer { get; set; }

	private AudioSource Speaker { get; set; }

	private GameObject PadSmokeContainer { get; set; }

	private ParticleSystem PadSmoke { get; set; }

	void Start()
	{
		LaunchPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
		RB = GetComponent<Rigidbody>();

		string xml = File.ReadAllText("launch_speeds.xml");

		SpeedInfoList = XMLDeserialize<List<SpeedInfo>>(xml);

		List<AudioSource> audio_sources = GetComponents<AudioSource>().ToList();

		IgnitionSound = audio_sources.Where(it => it.clip.name == "main_engine_ignition").First();
		BurnSound = audio_sources.Where(it => it.clip.name == "main_engine_burn").First();

		IntroPlayer = GameObject.Find("IntroPlayer").GetComponent<AudioSource>();
		OutroPlayer = GameObject.Find("OutroPlayer").GetComponent<AudioSource>();

		Speaker = GameObject.Find("Speaker").GetComponent<AudioSource>();

		PadSmokeContainer = GameObject.Find("whirlingSmoke");
		PadSmoke = PadSmokeContainer.GetComponent<ParticleSystem>(); ;
		PadSmokeContainer.SetActive(false);

	}

	private bool PlayerStarted { get; set; }
	private Single TimeOffset { get; set; }

	private DateTime Launched { get; set; }

	private bool Ignition { get; set; }
	private bool IgnitionSet { get; set; }
	private bool IgnitionPlayed { get; set; }

	private bool Burn { get; set; }
	private bool BurnSet { get; set; }
	private bool BurnPlayed { get; set; }

	private bool Outro { get; set; }
	private bool OutroSet { get; set; }
	private bool OutroPlayed { get; set; }

	private Single TimeSincePlayerStarted { get; set; }

	void Update()
	{

		if (Input.GetKeyDown("space") && !PlayerStarted)
		{
			TimeSincePlayerStarted = Time.timeSinceLevelLoad;
			PlayerStarted = true;
			TimeOffset = Time.timeSinceLevelLoad;
			IntroPlayer.Stop();
			Speaker.Play();
			PadSmokeContainer.SetActive(true);
		}

		ProcessStateMachine();

		if (Ignition && !IgnitionPlayed)
		{
			IgnitionPlayed = true;
			IgnitionSound.Play();
		}

		if (Burn && !BurnPlayed)
		{
			BurnPlayed = true;
			BurnSound.Play();
		}

		ProcessGravityTurn();


		Single angle = gameObject.transform.rotation.eulerAngles.x;
		print(angle);
		if (angle >= 15F && !OutroPlayed)
		{
			OutroPlayer.Play();
			OutroPlayed = true;
			print("STARTED AUDIO");
			
		}

		if (OutroPlayed && !OutroPlayer.isPlaying)
		{

			Application.Quit();
		}
	}

	private void ProcessStateMachine()
	{
		if (Time.timeSinceLevelLoad >= 6 + TimeOffset && !IgnitionSet && PlayerStarted)
		{
			Ignition = true;
			IgnitionSet = true;
		}

		if (Time.timeSinceLevelLoad >= 11 + TimeOffset && !BurnSet && PlayerStarted)
		{
			Burn = true;
			BurnSet = true;
		}
	}

	private void ProcessGravityTurn()
	{
		if (Time.timeSinceLevelLoad >= TimeSincePlayerStarted + StartAt && transform.rotation.eulerAngles.x < MaxDegrees && PlayerStarted)
		{
			PadSmoke.Stop();
			transform.Rotate(0.75F * Time.deltaTime, 0, 0);
		}
	}

	private int VelocityCounter { get; set; }

	List<SpeedInfo> SpeedInfoList { get; set; }

	private bool EngineStopped { get; set; }

	void FixedUpdate()
	{
		if (Burn)
		{
			Boost();
		}
		else
		{
			if (!EngineStopped)
			{
				StopEngine();
			}
		}
	}

	private void StopEngine()
	{
		RB.velocity = new Vector3(0, 0, 0);
		EngineStopped = true;
	}

	private void Boost()
	{
		Single velocity = SpeedInfoList[VelocityCounter].Speed;
		RB.velocity = transform.rotation * new Vector3(0, velocity, 0);
		VelocityCounter++;
		if (Time.timeSinceLevelLoad > 168)
		{
			Burn = false;
		}
	}

	#region Helpers

	private T XMLDeserialize<T>(string content)
	{
		XmlSerializer xml = new XmlSerializer(typeof(T));
		T result;
		using (MemoryStream ms = new MemoryStream(UTF8Encoding.Default.GetBytes(content)))
		{
			result = (T)xml.Deserialize(ms);
			ms.Close();
		}
		return result;
	}

	#endregion

}
